//
//  ModalViewController.swift
//  DesignExample
//
//  Created by Olzhas Akhmetov on 05.04.2021.
//

import UIKit

protocol SelectCategoryProtocol {
    func setCategory(_ category : String)
}

class ModalViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    var categoryArray : [String] = []
    
    var delegate : SelectCategoryProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.layer.cornerRadius = 6.0
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissView))
        tap.delegate = self
        view.addGestureRecognizer(tap)
        
        if Int(view.frame.height - topConstraint.constant - bottomConstraint.constant) > categoryArray.count * 44 {
            let constraint = (Int(view.frame.height) - categoryArray.count * 44) / 2
            topConstraint.constant = CGFloat(constraint)
            bottomConstraint.constant = CGFloat(constraint)
        }
    }
    
    @objc func dismissView() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if (touch.view?.isDescendant(of: tableView))! {
            return false
        }
        return true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoryArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
        
        cell.textLabel?.text = categoryArray[indexPath.row]
        
        cell.textLabel?.font = UIFont(name: "SFProDisplay-Bold", size: 15.0)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.setCategory(categoryArray[indexPath.row])
        dismissView()
    }

}
