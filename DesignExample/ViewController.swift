//
//  ViewController.swift
//  DesignExample
//
//  Created by Olzhas Akhmetov on 02.04.2021.
//

import UIKit

class ViewController: UIViewController, SelectCategoryProtocol {
    
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var categoryButton: UIButton!
    
    //["Electronics","Clothing","Service","Food","Food"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        configureKeyboardSettings()
        
        let contactRect = CGRect(x: 0, y: 0, width: bottomView.frame.width, height: 2.0)
        bottomView.layer.shadowColor = UIColor(red: 0.004, green: 0.122, blue: 0.247, alpha: 0.05).cgColor
        bottomView.layer.shadowPath = UIBezierPath(ovalIn: contactRect).cgPath
        bottomView.layer.shadowRadius = 20
        bottomView.layer.shadowOpacity = 1

        bottomView.layer.shadowPath = UIBezierPath(rect: bottomView.bounds).cgPath
    }

    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if bottomConstraint.constant == 20.0 {
                bottomConstraint.constant = keyboardSize.height
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if bottomConstraint.constant != 20.0 {
            bottomConstraint.constant = 20.0
        }
    }
    
    fileprivate func configureKeyboardSettings() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        hideKeyboardWhenTappedAround()
    }
    
    @IBAction func selectCategoryTouch(_ sender: Any) {
        
        let viewcontroller = storyboard?.instantiateViewController(identifier: "ModalViewController") as! ModalViewController
        
        viewcontroller.modalPresentationStyle = .overCurrentContext
        
        viewcontroller.delegate = self
        
        viewcontroller.categoryArray = ["Electronics",  "Clothing", "Service", "Food", "Other"]
        
        self.present(viewcontroller, animated: true, completion: nil)
        
    }
    
    func setCategory(_ category: String) {
        categoryButton.setTitle(category, for: .normal)
    }

}

